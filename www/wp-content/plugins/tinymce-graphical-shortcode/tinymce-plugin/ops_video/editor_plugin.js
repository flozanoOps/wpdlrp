
(function() {
  //var OPS_URL= "http://192.168.0.150/dev/nicolas/dlrp/trunk/www/";
  var OPS_URL= "http://http://test-dlrp.opsismedia.com/";
	tinymce.create('tinymce.plugins.ops_video', {

		init : function(ed, url) {
			var t = this;

			t.url = url;



			//replace shortcode before editor content set
           //			ed.onBeforeSetContent.add(function(ed, o) {
           //				o.content = t._do_spot(o.content);
           //			});
            ed.on('BeforeSetcontent', function(e){
                  if ( e.content )
                    e.content = t._do_spot(e.content);
            });

			//replace shortcode as its inserted into editor (which uses the exec command)
//			ed.onExecCommand.add(function(ed, cmd) {
//			    if (cmd ==='mceInsertContent'){
//					tinyMCE.activeEditor.setContent( t._do_spot(tinyMCE.activeEditor.getContent()) );
//				}
//			});
            ed.on( 'ExecCommand', function( e ) {
                if (e.command ==='mceInsertContent'){
                    tinyMCE.activeEditor.setContent( t._do_spot(tinyMCE.activeEditor.getContent()) );
                }
            });

                   //replace the image back to shortcode on save
//			ed.onPostProcess.add(function(ed, o) {
//				if (o.get)
//					o.content = t._get_spot(o.content);
//			});
           ed.on( 'PostProcess', function( e ) {
                if ( e.get ) {
                     e.content = t._get_spot(e.content);
                }
            });

		},

/*		_do_spot : function(co) {
			return co.replace(/\[ops_video([^\]]*)\]/g, function(a,b){
				return '<img src="../wp-content/plugins/tinymce-graphical-shortcode/tinymce-plugin/ops_video/images/t.gif" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
			});
		},*/
/*      	_do_spot : function(co) {
           if(co.match(/\[ops_video\]/g)){
                console.log('ok');
                return co.replace(/\[ops_video()\]/g, function(a,b){
             		console.log(co);
                 	console.log(b);
                 	return '[ops_video]';
                });
           } else if(co.match(/\[ops_video id=([0-9]+)\]/g)){

            	return co.replace(/\[ops_video([^\]]*)\]/g, function(a,b){
                 	console.log(a);
                 	var id_raw = a.toString();
                 	var id = id_raw.slice(14,17);
                 	console.log(id);
                 	return '<img src="'+OPS_URL+'makeVignette.php?image=/storyboard/0000/0341//P003_test_AAAF_vis_00010000.jpg&w=720&ol=player" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
                 	//return '<img src="http://192.168.0.150/dev/nicolas/dlrp/trunk/www/player_export.php?id='+id+'&play=false" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
                 	//return '<img src="http://192.168.0.150/dev/nicolas/dlrp/trunk/www/player_export.php?id='+id+'&play=false" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
                 	//return '<img src="../wp-content/plugins/tinymce-graphical-shortcode/tinymce-plugin/ops_video/images/t.gif" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
                });
       		}*/
    _do_spot : function(co) {
      if(co.match(/\[ops_video([^\]]*)\]/g)) {
        console.log('ok');
        return co.replace(/\[ops_video([^\]]*)\]/g, function(a,b) {
          console.log(co);
          console.log(b);
          if (b=='') {
            return '[ops_video]';
          } else {
            console.log(b);
            if(b.match(/id=(\d+)/g)) {
                return b.replace(/id=(\d+)/g, function(c,d) {
                console.log(d);
                return '<img src="'+OPS_URL+'makeVignette.php?image='+d+'&type=doc&w=720&ol=player" class="wpSpot mceItem" title="ops_video'+tinymce.DOM.encode(b)+'" />';
              });
            }
          }
        });
      }
    },

		_get_spot : function(co) {

			function getAttr(s, n) {
				n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
				return n ? tinymce.DOM.decode(n[1]) : '';
			};

			return co.replace(/(?:<p[^>]*>)*(<img[^>]+>)(?:<\/p>)*/g, function(a,im) {
				var cls = getAttr(im, 'class');

				if ( cls.indexOf('wpSpot') != -1 )
					return '<p>['+tinymce.trim(getAttr(im, 'title'))+']</p>';

				return a;
			});
		},

		getInfo : function() {
			return {
				longname : 'Spots shortcode replace',
				author : 'Simon Dunton',
				authorurl : 'http://www.wpsites.co.uk',
				infourl : '',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('ops_video', tinymce.plugins.ops_video);
})();
