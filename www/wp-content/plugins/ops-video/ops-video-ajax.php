<?php
//define("OPS_URL","http://192.168.0.150/dev/nicolas/dlrp/trunk/www");
//define("OPS_URL","http://test-dlrp.opsismedia.com");
require_once (dirname(__FILE__) . '/ops-video-constantes.php');
//require_once(dirname(__FILE__) . '/ops-functions.php');

require_once('../../../wp-config.php');

$language=blogLanguage();


/*add_action( 'wp_ajax_my_action', 'my_action_callback' );

function my_action_callback() {
    alert('test');

    global $wpdb;
    $current_user = wp_get_current_user();
    //console.log(popID);

    //$video_id='<script>document.write(popID);</script>';
                        var_dump($video_id);
    var_dump('test');
    $response=$wpdb->insert(
        'dlp_analytics',
        array(
            'user_id' => $current_user->data->ID,
            'blog_id' => get_current_blog_id() ,
            'attachment_id' => $video_id,
            //'datetime' => current_time('timestamp') ,
            'action' => 'View video'
        ),
        array(
            '%d',
            '%d',
            '%d',
            '%s'

        )
    );
}*/


    if ($_POST['action'] =='video_view' || $_POST['action'] =='video_download' || $_POST['action'] =='video_add_to_folder') {
        global $wpdb;
        if(!isset($wpdb)) {
            require_once('../../../wp-includes/wp-db.php');
        }

        $response=$wpdb->insert(
            'dlp_analytics',
            array(
                'user_id'       => $_POST['user_id'],
                'blog_id'       => $_POST['blog_id'] ,
                'attachment_id' => $_POST['id_video'],
                'action'        => $_POST['action'],
                'datetime'      => current_time('mysql', 1)
            ),
            array(
                '%d',
                '%d',
                '%d',
                '%s',
                '%s'
            )
        );

        $myRow = $wpdb->get_row( "SELECT ID FROM dlp_videos WHERE video_id = ".intval($_POST['id_video'])." AND blog_id = ".intval($_POST['blog_id']) );
        if ( $myRow !== null ) { //Update du nom de la video
            $response2=$wpdb->update(
                'dlp_videos',
                array(
                    'content_name' => trim(strip_tags($_POST['title']))
                ),
                array( 'ID' => $myRow->ID ),
                array(
                    '%s'
                ),
                array( '%d' )
            );
        } else {
            $response2=$wpdb->insert(
                'dlp_videos',
                array(
                    'video_id'      => $_POST['id_video'],
                    'blog_id'       => $_POST['blog_id'] ,
                    'content_name'  => trim(strip_tags($_POST['title']))
                ),
                array(
                    '%d',
                    '%d',
                    '%s'
                )
            );
        }

        echo $response.'-'.$response2;

    }


    if ($_POST['action'] =='video_filter' || $_POST['action'] =='video_filter_fo') {

        $param['chFields'][0] = "DOC_ID_MEDIA";
        $param['chTypes'][0] = "CE";
        $param['chValues'][0] = "V";

        if (isset($_POST['cat']) && $_POST['cat']!='') {
            $param['chFields'][1] = "CAT";
            $param['chTypes'][1] = "VI";
            $param['chValues'][1] = $_POST['cat'];//V_CAT
        } else {
            $_POST['cat']='';
        }
        if (isset($_POST['full_text']) && $_POST['full_text']!='') {
            $param['chFields'][2] = "DOC_TITRE,DOC_SOUSTITRE,DOC_TITREORI,DOC_TITRE_COL,DOC_AUTRE_TITRE,DOC_RES,DOC_SEQ,DOC_NOTES,DOC_RECOMP,DOC_ACC,DOC_COMMENT,DOC_RES_CAT,DOC_LEX,DOC_VAL,DOC_XML";
            $param['chTypes'][2] = "FT";
            $param['chValues'][2] = $_POST['full_text'];//V
            $param['chNoHLs'][2] = true;
        } else {
            $_POST['full_text']='';
        }

        $today=date("Y-m-d");
        $param['chFields'][3] = "DOC_DATE_DROIT_DEB";
        $param['chTypes'][3] = "CDN";
        $param['chValues'][3] = $today;
        $param['chValues2'][3] = "<=";

        $param['chFields'][4] = "DOC_DATE_DROIT_FIN";
        $param['chTypes'][4] = "CDN";
        $param['chValues'][4] = $today;
        $param['chValues2'][4] = ">=";

        $param['tri'] = "doc_date_prod1";
        $param['nbLignes'] = NBR_LIGNES;//120
        $param["page"] = $_POST['page'];

        $str_post = http_build_query($param);
/*        print_r($str_post);
        exit;*/

        //$url=WPURLS.OPS_URL;
        //var_dump($url);
        if (isset($_POST['lang']) && $_POST['lang'] !='') {
            $curl=curl_init(OPS_URL."/service.php?urlaction=recherche&lang=".$_POST['lang']);
        } else {
            $curl=curl_init(OPS_URL."/service.php?urlaction=recherche");
        }
        //var_dump(OPS_URL."/service.php?urlaction=recherche");

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);


        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);
/*        echo "<pre align='left'>";
        var_dump($rsp);
        echo "</pre>";
        exit;*/

        $rows=[];
        if (!empty($rsp['data']['rows'])){
            if (array_key_exists("id_doc", $rsp['data']['rows']['row'])) {
                $rows[0]=$rsp['data']['rows']['row'];

            } else {
                $rows=$rsp['data']['rows']['row'];
            }
        }
/*        echo "<pre align='left'>";
        var_dump($rows);
        echo "</pre>";*/
        //print_r($rows);
        //var_dump($rows);
        //return $rows;

        $html= '';
        if (!empty($rows)){
            if ($_POST['action'] =='video_filter'){

                foreach ($rows as $idx=>$row) {

                    if($idx==0){ $class="thumb selected";}
                    else {$class="thumb";}

                    if (empty($row['vignette'])) { //si pas de vignette
                        $row['vignette']='';
                        $class.=' blank_bo';
                        $vignette =OPS_URL.'/design/images/vignette_blank.png';
                    } else {
                        $vignette =OPS_URL.'/makeVignette.php?image='.$row['vignette'].'&w=160&h=90&kr=1';
                    }

                    $html.= '<li id="ops-thumb-'.($idx+1).'">';
                    $html.= '<img src="'.$vignette.'" id="'.$row['id_doc'].'" class="'.$class.'" title="'.$row['doc_titre'].'"/>';
                    $html.= '<p>'.$row['doc_titre'].'</p>';
                    $html.= '</li>';

                }
                $html.= '<input type="hidden" id ="nb_page_result" name="nb_page" value="'.$rsp['data']['nb_pages'].'" />'; // ajout d'une variable caché sur le nbr de page de résultat

            } else if ($_POST['action'] =='video_filter_fo'){
                if ( is_user_logged_in() ) {
                    $current_user = wp_get_current_user();
                    $current_user_email = $current_user->data->user_email;
                } else {
                    $current_user_email='';
                }

                foreach ($rows as $idx=>$row) {

                    $html.= '<div id="ops-thumb-'.($idx+1).'">';
                    $hash_key=md5($row['id_doc'].$language.FROM.PLAYER_EXPORT_SECRET_KEY); // clé de securisation du player_export

                    if (empty($row['vignette'])) { //si pas de vignette
                        $row['vignette']='';
                        $class=' video_disabled';
                        $vignette =OPS_URL.'/design/images/vignette_blank.png';
                    } else {
                        $vignette =OPS_URL.'/makeVignette.php?image='.$row['vignette'].'&w=214&h=120&kr=1&ol=player';
                        $class=' video_enabled';
                    }

                    $html.= '<img src="'.$vignette.'" id="'.$row['id_doc'].'" class="thumbnail'.$class.'" alt="'.$row['doc_titre'].'" title="'.$row['doc_titre'].'" data-hash="'.$hash_key.'" data-email="'.$current_user_email.'" />';
                    $html.= '<div class="titre_vignette">'.$row['doc_titre'].'</div>';
                    $html.= '</div>';

                }
                $html.= '<input type="hidden" id ="nb_page_result" name="nb_page" value="'.$rsp['data']['nb_pages'].'" />'; // ajout d'une variable caché sur le nbr de page de résultat

            }
        }
        echo $html;
    }
