(function ($) {
	insertOpsShortcode = function(name) {
			var win = window.dialogArguments || opener || parent || top;

			var shortcode='[ops_video id='+name+']';
			win.send_to_editor(shortcode);
			win.send_to_editor('<br/> <br/><br/>');//attention espace après <br/> indispensable au saut de ligne dans TinyMCE

		}

	$(function () {
		$('#ops-thumbs').delegate('img','click', function(){
			$('#ops-thumbs li').removeClass("selected");
			$(this).parent().addClass("selected");
		});
		$('#insert_shortcode').bind('click',function() {
			var myId = $('li.selected img').attr('id');
            if(myId!=undefined) insertOpsShortcode(myId);
		});


		$(document).ready(function() {
	      	$(' #filter_media_form , #prev-result , #next-result ').on('click', function(event) { // gestion du formulaire de selection de video et de la pagination des resultats
            	event.preventDefault();
		        var page = 1; // cas par défaut pour afficher la première page de résultat (cas lors du clic sur "Filtrer")
		        var cat = $('#select_cat option:selected').val();
		        cat = (cat=='ALL') ? '' : cat;
		        var keyword = $('#filtre_video #select_text').val();
		        //console.log('keywxord : '+keyword);
		        if (this.id == 'next-result') {
		        	var page = parseInt($('#id-result-page').val())+1;
	        	} else if (this.id == 'prev-result') {
		        	var page = parseInt($('#id-result-page').val())-1;
	        	}
	        	var maxPage = $('#count-page').text();
	        	if (page !=1 && page==maxPage){
	        		$('#prev-result').css("visibility","visible");
	        		$('#next-result').css("visibility","hidden");
	        	} else if (page !=1 && page!=maxPage){
	        		$('#prev-result').css("visibility","visible");
	        		$('#next-result').css("visibility","visible");
	        	} else {
	        		$('#prev-result').css("visibility","hidden");
	        		$('#next-result').css("visibility","visible");
	        	}
	        	//console.log("page : "+page);
		        $.post(
		            WPURLS.plugin_url+'/ops-video/ops-video-ajax.php',
		            {
		                'action'        : 'video_filter',
		                'cat'			: cat,
		                'full_text'		: keyword,
		                'page'			: page,
		                'lang'			: WPURLS.CURRENT_LANG
		            },
		            function(response){ // control du retour - si 0 activité non enregistré
		                if (response) {
	                      //console.log('nbr de page dans la réponse : '+$(response).filter("#nb_page_result").val());
	                      $("#ops-thumbs").html(response);
	                      $("#id-result-page").val(page);
	                      $("#current-page").text(page);
	                      if ($(response).filter("#nb_page_result").val()=='1'){ // si une page de résultat, on n'affiche pas la table de pagination, correction du au fait que l'on puisse valider en cliquant sur les flèches de pagination
	                          $("#nav-table").css("display","none");
	                      } else { // si plus d'une page de résultat, correction du au fait que l'on puisse valider en cliquant sur les flèches de pagination
	                        if (page === 1 && $(response).find('img').length < WPURLS.NBR_LIGNES) {
	                            $("#nav-table").css("display","none");
	                        } else if ($(response).find('img').length <= WPURLS.NBR_LIGNES){
	                            $("#nav-table").css("display","table");
	                        }
	                      }
	                    } else {
	                      $("#ops-thumbs").html("Pas de résultats");
	                      $("#nav-table").css("display","none");
	                    }
		            }
		        );
		        return true;
		    });
	    });

	});
})(jQuery);
