<?php

//define(OPS_URL,"http://192.168.0.150/int/dlrp/www");
//define("OPS_URL","http://192.168.0.150/dev/nicolas/dlrp/trunk/www");
//define("OPS_URL","http://test-dlrp.opsismedia.com");
define("PLAYER_EXPORT_SECRET_KEY","testwp");
define("NBR_LIGNES", 15);

$str_lang['FR']['search']='Rechercher';
$str_lang['FR']['category']='Catégorie';
$str_lang['FR']['filter']='Filtrer';
$str_lang['FR']['next_page']='Page suivante';
$str_lang['FR']['previous_page']='Page précédente';
$str_lang['FR']['all']='Toutes';

$str_lang['EN']['search']='Search';
$str_lang['EN']['category']='Category';
$str_lang['EN']['filter']='Filter';
$str_lang['EN']['next_page']='Next page';
$str_lang['EN']['previous_page']='Previous page';
$str_lang['EN']['all']='All';

$str_lang['DE']['search']='Suche';
$str_lang['DE']['category']='Kategorie';
$str_lang['DE']['filter']='Filter';
$str_lang['DE']['next_page']='Nächste Seite';
$str_lang['DE']['previous_page']='Vorherige Seite';
$str_lang['DE']['all']='Alle';

$str_lang['IT']['search']='Cerca';
$str_lang['IT']['category']='Categoria';
$str_lang['IT']['filter']='Filtra';
$str_lang['IT']['next_page']='Pagina prossimo';
$str_lang['IT']['previous_page']='Pagina precedente';
$str_lang['IT']['all']='Tutte';

$str_lang['ES']['search']='Buscar';
$str_lang['ES']['category']='Categoría';
$str_lang['ES']['filter']='Filtrar';
$str_lang['ES']['next_page']='Página siguiente';
$str_lang['ES']['previous_page']='Página anterior';
$str_lang['ES']['all']='Todas';

$str_lang['NL']['search']='Zoeken';
$str_lang['NL']['category']='Categorie';
$str_lang['NL']['filter']='Filter';
$str_lang['NL']['next_page']='Volgende pagina';
$str_lang['NL']['previous_page']='Vorige pagina';
$str_lang['NL']['all']='Alle';

