<?php

    //Assign a name to your tab
    function upload_ops_media_menu($tabs) {
        $tabs['ops_upload']='Insert from VOD';
        return $tabs;
    }

    //Adds your scripts to your plugin
    function upload_ops_scripts() {
        //Adds css
        $myStyleUrl = plugins_url('ops-upload-media.css', __FILE__);
        wp_register_style('myStyleSheets', $myStyleUrl);
        wp_enqueue_style( 'myStyleSheets');

        //Adds your custom script
        wp_deregister_script( 'upload-media-script' );
        //wp_register_script( 'upload-media-script', '/wp-content/plugins/upload-media/ops-upload-media.js');
        wp_register_script( 'upload-media-script', plugins_url('ops-upload-media.js', __FILE__));
        wp_localize_script('upload-media-script', 'WPURLS', array( 'siteurl' => get_option('siteurl'), 'plugin_url' => plugins_url(), 'OPS_URL' => OPS_URL, 'NBR_LIGNES' => NBR_LIGNES, 'CURRENT_LANG' => blogLanguage(), 'No_result'=> __('No results')));
        wp_enqueue_script( 'upload-media-script' );
    }

    //This is our form for the plugin
    function upload_ops_upload_form () {
        //echos the tabs at the top of the media window
        media_upload_header();

        //Adds your javascript
        upload_ops_scripts();

        //formulaire de filtre
        $cats=ops_getCat();

        echo'<div id="filtre_video">';
        echo'<label for="select_text" style="margin-right:9px;">Recherche</label><input type="text" id="select_text" />';
        echo'<label for="select_cat" style="margin: 0 15px;">Catégorie</label>';
        echo '<select id="select_cat">';
        echo '<option value="ALL" selected>Toutes</option>';
        foreach ($cats as $key => $cat) {
            echo '<option value="'.$cat['id_val'].'">'.$cat['value'].'</option>';
        }
        echo '</select>';
        echo "<input type='hidden' name='page' id='id-result-page' value='1' />";
        echo '<input type="submit" value="Filtrer" id="filter_media_form" class="button" style="margin: 1px 24px;" />';
        echo '</div>';
        $data=ops_getAllVideos();
        $rows=$data['rows']['row'];
        echo '<div>
        <ul id="ops-thumbs">';

        foreach ($rows as $idx=>$row) {
            if($idx==0){ $class="thumb selected";}
            else {$class="thumb";}
            //if ($row['vignette'] !='storyboard/ / '){
                if (empty($row['vignette'])) { //si pas de vignette
                    $row['vignette']='';
                    $vignette =OPS_URL.'/design/images/vignette_blank.png';
                    $class.=" blank_bo";
                } else {
                    $vignette =OPS_URL.'/makeVignette.php?image='.$row['vignette'].'&w=160&h=90&kr=1';
                }
                echo '<li id="ops-thumb-'.($idx+1).'">';
                echo '<img src="'.$vignette.'" id="'.$row['id_doc'].'" class="'.$class.'" title="'.$row['doc_titre'].'"/>';
                echo '<p>'.$row['doc_titre'].'</p>';
                echo '</li>';
            //}
       }
        echo '</ul>
        </div>';

        if ($data['nbRows'] > NBR_LIGNES ) {
            echo '<table id="nav-table" width="100%">
                    <tr>
                        <td width="33%"></td>
                        <td width="33%" style="text-align:center;">
                            <a href="#" title="Page précédente" style="visibility:hidden;" id="prev-result"><span class="prev-icon"></span></a>
                            <span id="current-page">1</span>/<span id="count-page">'.$data['nb_pages'].'</span>
                            <a href="#" title="Page suivante" id="next-result"><span class="next-icon"></span></a>
                        </td>
                        <td width="33%"></td>
                    </tr>
                </table>';
        }

        echo "<div class='insert-form'>
                <input id='insert_shortcode' type='button' class='button' value='Insert Video' style='margin-left: 25px' />
            </div> ";

    }

    //Returns the iframe that your plugin will be returned in
    function upload_ops_menu_handle() {
        return wp_iframe('upload_ops_upload_form');
    }

    //Needed script to make sure wordpresses media upload scripts are inplace

    function upload_ops_back(){
        wp_enqueue_script('media-upload');
    }

    //Adds your tab to the media upload button
    add_filter('media_upload_tabs', 'upload_ops_media_menu');

    //Adds your menu handle to when the media upload action occurs
    add_action('media_upload_ops_upload', 'upload_ops_menu_handle');
    add_action('wp_enqueue_scripts', 'upload_ops_back');

