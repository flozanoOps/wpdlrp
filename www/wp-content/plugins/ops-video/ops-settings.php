<?php

// Register settings page under the WordPress settings menu
add_action( 'admin_menu', 'ops_video_menu' );
function ops_video_menu() {
	add_options_page( __('Ops Video Options','menu-ops'), __('Ops Video Options','menu-ops'), 'manage_options', 'ops-video-settings', 'ops_video_admin' );
	add_action( 'admin_init', 'register_ops_settings' );
}

// Render settings page
function ops_video_admin() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
	<div class="wrap">
	<h2>Ops Video</h2>
	<form method="post" action="options.php">
		<?php settings_fields( 'ops-settings-group' ); ?>
		<?php do_settings_sections( 'ops_video' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">
					<label for="ops_video_url">Opsis Media URL</label>
				</th>
				<td>
					<input type="text" class="regular-text" name="ops_video_url" id="ops_video_url" value="<?php echo get_option('ops_video_url') ?>">
                    <span class="description">e.g. http://dlrp.opsismedia.com</span>
				</td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
	</form>
	</div>
	<?php
}

// Register OPS settings with WordPress
function register_ops_settings() {
	register_setting( 'ops-settings-group', 'ops_video_url' );
	add_settings_section( 'ops_video', 'Ops Video Settings', 'f_ops_video_settings_section', 'ops_video' );
}

// Render section content for OPS section
function f_ops_video_settings_section() {
	?>
	<p>
		Provide connection URL for Opsis Media site.
	</p>
	<?php
}
?>
