jQuery(function($) {
  //console.log('test2');
    $(document).ready(function(){
        $('#insert-ops-video').click(open_ops_video_window);
        //alert('test');
        //console.log('test');

      $(' #filter_media_form_fo , #prev-result , #next-result ').on('click', function(event) { // gestion du formulaire de selection de video et de la pagination des resultats
            event.preventDefault();
            var page = 1; // cas par défaut pour afficher la première page de résultat (cas lors du clic sur "Filtrer")
            var cat = $('#select_cat option:selected').val();
            cat = (cat=='ALL') ? '' : cat;
            var keyword = $('#filtre_video #select_text').val();
              if (this.id == 'next-result') {
                var page = parseInt($('#id-result-page').val())+1;
              } else if (this.id == 'prev-result') {
                var page = parseInt($('#id-result-page').val())-1;
              }
            var maxPage = $('#count-page').text();
            if (page !=1 && page==maxPage){
              $('#prev-result').css("visibility","visible");
              $('#next-result').css("visibility","hidden");
            } else if (page !=1 && page!=maxPage){
              $('#prev-result').css("visibility","visible");
              $('#next-result').css("visibility","visible");
            } else {
              $('#prev-result').css("visibility","hidden");
              $('#next-result').css("visibility","visible");
            }
            //console.log("page : "+page);
            $.post(
                WPURLS.plugin_url+'/ops-video/ops-video-ajax.php',
                {
                    'action'    : 'video_filter_fo',
                    'cat'       : cat,
                    'full_text' : keyword,
                    'page'      : page,
                    'lang'      : WPURLS.CURRENT_LANG

                },
                function(response){ // control du retour - si 0 activité non enregistré
                    if (response) {
                      //console.log('nbr de page dans la réponse : '+$(response).filter("#nb_page_result").val());
                      $("#ops-thumbs").html(response);
                      $("#id-result-page").val(page);
                      $("#current-page").text(page);
                      if ($(response).filter("#nb_page_result").val()=='1'){ // si une page de résultat, on n'affiche pas la table de pagination, correction du au fait que l'on puisse valider en cliquant sur les flèches de pagination
                          $("#nav-table").css("display","none");
                      } else { // si plus d'une page de résultat, correction du au fait que l'on puisse valider en cliquant sur les flèches de pagination
                        if (page === 1 && $(response).find('img').length < WPURLS.NBR_LIGNES) {
                            $("#nav-table").css("display","none");
                        } else if ($(response).find('img').length <= WPURLS.NBR_LIGNES){
                            $("#nav-table").css("display","table");
                        }
                      }
                    } else {
                      //$("#ops-thumbs").html("Pas de résultats");
                      var mes = WPURLS.No_result;
                      $("#ops-thumbs").html(mes);
                      $("#nav-table").css("display","none");
                    }
                }
            );
            return true;
        });
    });


    function open_ops_video_window() {
       if (this.window === undefined) {
       this.window = wp.media({
                              title: 'Insert a media',
                              library: {type: 'image'},
                              multiple: false,
                              button: {text: 'Insert'}
                              });

       var self = this; // Needed to retrieve our variable in the anonymous function below
       this.window.on('select', function() {
                      var first = self.window.state().get('selection').first().toJSON();
                      wp.media.editor.insert('[ops_video id="' + first.id + '"]');
                      });
       }

       this.window.open();
       return false;
    }

});