<?php


//require_once('../../../../wp-config.php');

/* fonction de récupération de la langue utilisé par le blog WP */
function blogLanguage(){
    if (class_exists('MslsBlogCollection')){
         $blog     = MslsBlogCollection::instance()->get_current_blog();
        $language = strtoupper($blog->get_language());
    } else {
        //$language = 'FR';
        $language = get_bloginfo('language');
    }
/*    var_dump($language);
    exit;*/
    $language=strtoupper(substr($language,0,2));

    $array_langues=array('FR','EN','DE','IT','ES','NL');
    if (in_array($language, $array_langues)){
        return $language;
    } else {
        return 'EN';
    }
}


/* fonction de traduction de wording defini dans ops-video-constantes*/
function ops_lang($lang,$word) {
    global $str_lang;
    echo $str_lang[$lang][$word];
}

//-------------------------------------------------------------
// Ops Video by Opsomai
//-------------------------------------------------------------

/* fonctions de recupération de data de la plateforme dlrp */

/* recuperation de toutes les videos */
    function ops_getAllVideos(){
        $param['chFields'][0] = "DOC_ID_MEDIA";
        $param['chTypes'][0] = "CE";
        $param['chValues'][0] = "V";
        $today=date("Y-m-d");
        $param['chFields'][2] = "DOC_DATE_DROIT_DEB";
        $param['chTypes'][2] = "CDN";
        $param['chValues'][2] = $today;
        $param['chValues2'][2] = "<=";

        $param['chFields'][3] = "DOC_DATE_DROIT_FIN";
        $param['chTypes'][3] = "CDN";
        $param['chValues'][3] = $today;
        $param['chValues2'][3] = ">=";

        $param['tri'] = "doc_date_prod1";
        $langue = blogLanguage();
        $param['nbLignes'] = NBR_LIGNES;
        $str_post = http_build_query($param);


        $curl=curl_init(OPS_URL."/service.php?urlaction=recherche&lang=".$langue);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $data=$rsp['data'];
        return $data;
    }

/* recuperation d'un panier en fonction de l'email de son propriétaire */
    function ops_getPanier($email){
        $param['chFields'][0] = "DOC_ID_MEDIA";
        $param['chTypes'][0] = "CE";
        $param['chValues'][0] = "V";

        $today=date("Y-m-d");
        $param['chFields'][2] = "DOC_DATE_DROIT_DEB";
        $param['chTypes'][2] = "CDN";
        $param['chValues'][2] = $today;
        $param['chValues2'][2] = "<=";

        $param['chFields'][3] = "DOC_DATE_DROIT_FIN";
        $param['chTypes'][3] = "CDN";
        $param['chValues'][3] = $today;
        $param['chValues2'][3] = ">=";

        $param['tri'] = "doc_date_prod1";
        $param['email'] = $email;
        $langue = blogLanguage();
        $param['nbLignes'] = 100; //NBR_LIGNES;
        $str_post = http_build_query($param);

        $curl=curl_init(OPS_URL."/wp-panier.php?lang=".$langue);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $data=$rsp['data'];
        return $data;
    }

/* recupération d'une video */
    function ops_getVideo($id){
        $param['chFields'][0] = "DOC_ID_MEDIA";//
        $param['chTypes'][0] = "CE";//
        $param['chValues'][0] = "V";//
        $param['chNoHLs'][0] = "1";

        $param['chFields'][1] = "ID_DOC";//
        $param['chTypes'][1] = "CE";
        $param['chValues'][1] = $id;
        $param['chNoHLs'][1] = "1";
        
        $today=date("Y-m-d");
        $param['chFields'][2] = "DOC_DATE_DROIT_DEB";
        $param['chTypes'][2] = "CDN";
        $param['chValues'][2] = $today;
        $param['chValues2'][2] = "<=";
        $param['chNoHLs'][2] = "1";

        $param['chFields'][3] = "DOC_DATE_DROIT_FIN";
        $param['chTypes'][3] = "CDN";
        $param['chValues'][3] = $today;
        $param['chValues2'][3] = ">=";
        $param['chNoHLs'][3] = "1";

        $param['tri'] = "doc_date_prod1";
        $str_post = http_build_query($param);

        $langue = blogLanguage();
        $curl=curl_init(OPS_URL."/service.php?urlaction=recherche&lang=".$langue);//pas nécéssaire

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $rows=$rsp['data'];
        return $rows;
    }

/* recuperation des categories des video */

    function ops_getCat(){
        $param['nbLignes'] = "120"; //120
        $str_post = http_build_query($param);

        $curl=curl_init(OPS_URL."/service.php?urlaction=listevaleurs&type=V_CAT");

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $rows=$rsp['data']['rows']['row'];
        return $rows;
    }
