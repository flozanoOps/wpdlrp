<?php
//define("OPSIS_URL","http://192.168.0.150/dev/nicolas/dlrp/trunk/www");
//define("OPSIS_URL","http://test-dlrp.opsismedia.com");
require_once (dirname(__FILE__) . '/opsis-media-constantes.php');
//require_once(dirname(__FILE__) . '/opsis-functions.php');

require_once('../../../wp-config.php');

$language=opsis_blogLanguage();


    if ($_POST['action'] =='video_filter' || $_POST['action'] =='video_filter_fo') {

        if (isset($_POST['media']) && $_POST['media']!='') {
            $param['chField'][1] = "media";
            $param['chValue'][1] = strtoupper(substr($_POST['media'],0,1));
        } else {
            $_POST['media']='';
        }
        if (isset($_POST['full_text']) && $_POST['full_text']!='') {
            $param['chField'][2] = "texte";
            $param['chValue'][2] = $_POST['full_text'];//V
        } else {
            $_POST['full_text']='';
        }

        $param['tri'] = "doc_date_prod1";
        $param['nbLignes'] = NBR_LIGNES;//120
        $param["page"] = $_POST['page'];
		
		$param['api_key'] = OPSIS_KEY;

        $str_post = http_build_query($param);
/*        print_r($str_post);
        exit;*/

        //$url=WPURLS.OPSIS_URL;
        //var_dump($url);
        if (isset($_POST['lang']) && $_POST['lang'] !='') {
            $curl=curl_init(OPSIS_URL."/".OPSIS_API."?urlaction=recherche&lang=".$_POST['lang']);
        } else {
            $curl=curl_init(OPSIS_URL."/".OPSIS_API."?urlaction=recherche");
        }
        //var_dump(OPSIS_URL."/service.php?urlaction=recherche");

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);


        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);
/*        echo "<pre align='left'>";
        var_dump($rsp);
        echo "</pre>";
        exit;*/

        $rows=[];
        if (!empty($rsp['data']['rows'])){
            if (array_key_exists("code", $rsp['data']['rows']['row'])) {
                $rows[0]=$rsp['data']['rows']['row'];

            } else {
                $rows=$rsp['data']['rows']['row'];
            }
        }
/*        echo "<pre align='left'>";
        var_dump($rows);
        echo "</pre>";*/
        //print_r($rows);
        //var_dump($rows);
        //return $rows;

        $html= '';
        if (!empty($rows)){
            if ($_POST['action'] =='video_filter'){

                foreach ($rows as $idx=>$row) {

                    if($idx==0){ $class="thumb selected";}
                    else {$class="thumb";}

                    if (empty($row['vignette'])) { //si pas de vignette
                        $row['vignette']='';
                        $class.=' blank_bo';
                        $vignette =OPSIS_URL.'/design/images/vignette_blank.png';
                    } else {
						$vignette = substr($row['vignette'],strpos($row['vignette'],'/storyboard/'));
						$vignette = OPSIS_URL.'/makeVignette.php?image='.$vignette.'&w=160&h=90&kr=1';
                    }

                    $html.= '<li id="opsis-thumb-'.($idx+1).'">';
                    $html.= '<img src="'.$vignette.'" id="'.$row['code'].'" class="'.$class.'" title="'.$row['titre'].'"/>';
                    $html.= '<p>'.$row['titre'].'</p>';
                    $html.= '</li>';

                }
                $html.= '<input type="hidden" id ="nb_page_result" name="nb_page" value="'.$rsp['data']['nb_pages'].'" />'; // ajout d'une variable caché sur le nbr de page de résultat

            } else if ($_POST['action'] =='video_filter_fo'){
                if ( is_user_logged_in() ) {
                    $current_user = wp_get_current_user();
                    $current_user_email = $current_user->data->user_email;
                } else {
                    $current_user_email='';
                }

                foreach ($rows as $idx=>$row) {

                    $html.= '<div id="opsis-thumb-'.($idx+1).'">';
                    $hash_key=md5($row['code'].$language.FROM.PLAYER_EXPORT_SECRET_KEY); // clé de securisation du player_export

                    if (empty($row['vignette'])) { //si pas de vignette
                        $row['vignette']='';
                        $class=' video_disabled';
                        $vignette =OPSIS_URL.'/design/images/vignette_blank.png';
                    } else {
						$vignette = substr($row['vignette'],strpos($row['vignette'],'/storyboard/'));
						$vignette = OPSIS_URL.'/makeVignette.php?image='.$vignette.'&w=214&h=120&kr=1&ol=player';
                        $class=' video_enabled';
                    }

                    $html.= '<img src="'.$vignette.'" id="'.$row['code'].'" class="thumbnail'.$class.'" alt="'.$row['titre'].'" title="'.$row['titre'].'" data-hash="'.$hash_key.'" data-email="'.$current_user_email.'" />';
                    $html.= '<div class="titre_vignette">'.$row['titre'].'</div>';
                    $html.= '</div>';

                }
                $html.= '<input type="hidden" id ="nb_page_result" name="nb_page" value="'.$rsp['data']['nb_pages'].'" />'; // ajout d'une variable caché sur le nbr de page de résultat

            }
        }
        echo $html;
    }
