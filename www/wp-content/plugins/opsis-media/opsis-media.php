<?php
/*
Plugin Name: Opsis Media
Description: Media gallery
Author: Opsomai
Version: 1.0
Author URI: http://www.opsomai.com
*/


// Required PHP files
$PLUGIN_PATH = dirname(__FILE__).'/';
require_once($PLUGIN_PATH . '/opsis-settings.php');

$opsis_url = get_option('opsis_media_url');
$opsis_key = get_option('opsis_media_key');

if (empty($opsis_url) || empty($opsis_key)){
    return ;
}

if(substr($opsis_url,-4)==".php"){
	$opsis_api=basename($opsis_url);
	$opsis_url=dirname($opsis_url);
}else{
	$opsis_api="service.php";
}
	
	
define("OPSIS_URL",$opsis_url);
define("OPSIS_API",$opsis_api);
define("OPSIS_KEY",$opsis_key);

require_once($PLUGIN_PATH . '/opsis-functions.php');
require_once ($PLUGIN_PATH . '/opsis-media-constantes.php');
require_once ($PLUGIN_PATH . '/opsis-admin.php');



define("FROM",$_SERVER['SERVER_NAME']);



//----------------------------------------------------------------------
// Import Stylesheet
//----------------------------------------------------------------------

function add_opsis_stylesheets() {

	// registers your stylesheet
	wp_register_style( 'opsVideoStyles', plugins_url( 'opsis-media.css', __FILE__ ) );

    // loads your stylesheet
    wp_enqueue_style( 'opsVideoStyles' );

    wp_deregister_script( 'upload-media-script' );
    //wp_register_script( 'upload-media-script', '/wp-content/plugins/upload-media/opsis-upload-media.js');
    wp_enqueue_script('jquery');
    wp_register_script( 'upload-media-script', plugins_url('opsis-media.js', __FILE__));
    wp_localize_script('upload-media-script', 'WPURLS', array( 'siteurl' => get_option('siteurl'), 'plugin_url' => plugins_url(), 'OPSIS_URL' => OPSIS_URL, 'NBR_LIGNES' => NBR_LIGNES, 'CURRENT_LANG' => opsis_blogLanguage(), 'No_result'=> __('No results')));
    wp_enqueue_script( 'upload-media-script' );
}

add_action( 'wp_enqueue_scripts', 'add_opsis_stylesheets' );

/* test script conditionnel pour IE (styles css spécifiques au version d'ie)

add_action( 'wp_head', 'load_ie_styles' );
function load_ie_styles() {
  global $wp_styles;
  wp_register_style( 'ie-seulement', WP_PLUGIN_URL . '/opsis-media/style-ie.css' );
  $wp_styles->add_data( 'ie-seulement', 'conditional', 'gte IE 6' );
  wp_enqueue_style( 'ie-seulement' );
}*/

//---------------------------------------------------------------------------------------
// Enqueue jQuery from Google
//---------------------------------------------------------------------------------------

function opsis_enqueue_jquery() {

	wp_enqueue_script('jquery');

}

// Only add the javascript if we are NOT on the admin screen
add_action("wp_enqueue_scripts", "opsis_enqueue_jquery", 11);

//-------------------------------------------------
// Activate Opsis Media
//-------------------------------------------------

function activate_opsis_media(){

    $language=opsis_blogLanguage();

    ?>
    <script>

        //  Wrap the jQuery code in the generic function to allow use of
        //  the $ shortcut in WordPress's no-conflict jQuery environment

        ( function ($) {
            //affichage de la poppin au click sur une thumbnail de la mosaïque de videos
            $('#opsis-thumbs').delegate('img.video_enabled','click', function(){        // When someone clicks on a thumbnail

                if(typeof(myVideo)!="undefined" && myVideo != null) {
                    myVideo.StopTheVideo(); //stop la lecture d'une video précedemment lancée si encore en cours
                }

                var popID = $(this).attr('id');
                var hash = $(this).attr('data-hash');
                var user_email = $(this).attr('data-email');
                //var id_user = $(this).attr('data-user_id');
                var title = $(this).attr('title');

                //Faire apparaitre la video choisie dans l iframe avec le player par défaut ou le player d'un utilisateur connecté
                <?php

                    if (is_user_logged_in()){
                        $current_user = wp_get_current_user();
                        $user_id = $current_user->data->ID;
                    } else {
                        $user_id = 0;
                    }

                    $blog_id = get_current_blog_id();

                ?>

                var opsis_iframe = $('iframe.opsis_all_video');
                var opsis_container = opsis_iframe.parent();
                opsis_iframe.remove(); // très important pour garder une navigation UX correcte

                if (user_email!=''){ //connecté

                    opsis_iframe.attr('src','<?=OPSIS_URL?>/player_export.php?code='+popID+'&play=true&language=<?=$language?>&email='+user_email+'&h='+hash+'&r=<? echo FROM;?>&blog_id=<?=$blog_id?>&title='+title+'&user_id=<?=$user_id?>');
                    opsis_iframe.attr('style','opacity :1');
                    opsis_container.append(opsis_iframe);
                    //$('#opsis-main iframe.opsis_all_video').attr('height','549');

                } else { //non-connecté

                    opsis_iframe.attr('src', '<?=OPSIS_URL?>/player_export.php?code='+popID+'&play=true&language=<?=$language?>&h='+hash+'&r=<? echo FROM;?>&blog_id=<?=$blog_id?>&title='+title+'&user_id=<?=$user_id?>');
                    opsis_iframe.attr('style', 'opacity :1');
                    opsis_container.append(opsis_iframe);
                    //$('#opsis-main iframe.opsis_all_video').attr('height','405');

                }

                //Redimmensionnement de l'iframe en fonction de la taille du browser (responsive design)
                //Récupération du margin, qui permettra de centrer la fenêtre - au cas où, on ajuste en conformité avec le CSS
                if ($(window).width() > 955 ) { //si la width de la fenetre du browser est inférieure a 955px (responsive WP par défaut) (big)
                    var popMargTop = ($('iframe.opsis_all_video').height()) / 2;
                    var popMargLeft = ($('iframe.opsis_all_video').width()) / 2;
                    //popTop='50%';
                    //popLeft='50%';
                } else if ($('iframe.opsis_all_video').width() < $(window).width()) { //si l'iframe est plus petite que la fenetre du browser (small)
                    var popMargTop = ($('iframe.opsis_all_video').height()) / 2;
                    var popTop = '50%';
                    //var popMargLeft = ($('#opsis-main iframe').width()) / 2;
                    var popMargLeft = 0;
                    var popLeft = 25;
                    //var popMargLeft = ($('#opsis-main iframe').width() + 40) / 2; //si padding de 20px
                    $('iframe.opsis_all_video').width($(window).width() - 50);
                    //var popMargTop = ($('#opsis-main iframe').height() + 40) / 2; //si padding de 20px
                } else { // si l'iframe est plus grande que la fenetre du browser (very small)
                    var popMargTop = ($('iframe.opsis_all_video').height() / 2 - $(window).height() / 2 );
                    var popTop = '50%';
                    var popMargLeft = 0;
                    var popLeft = 10;
                    $('iframe.opsis_all_video').width($(window).width() - 20);
                    //$('#opsis-main iframe.opsis_all_video').height($(window).height() - 20);

                }
                //Apply Margin to Popup
                $('iframe.opsis_all_video').css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft,
                    'top' : popTop+'px',
                    'left' : popLeft+'px'
                });

                $('#opsis-main iframe.opsis_all_video').fadeIn();

                //Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) pour corriger les bogues de IE
                $('#fade').css({'filter' : 'alpha(opacity=90)'}).fadeToggle();

                if (user_email!=''){ // player connecté - apparition de l'icone de fermeture de poppin
                    //positionnement de l'icone pour fermer la poppin de visionnage
                    var btn_close_left = $(window).width()/2+$("iframe.opsis_all_video").width()/2 - 25;
                    var btn_close_top = $(window).height()/2-$("iframe.opsis_all_video").height()/2 + 6;
                    $("a.btn_close").css({'left': btn_close_left+'px','top': btn_close_top+'px'});
                    setTimeout(function(){
                        $('a.btn_close').fadeIn();
                    },1000);
                } else { // player non connecté - apparition de l'icone de fermeture de poppin
                    var btn_close_left = $(window).width()/2+$("iframe.opsis_all_video").width()/2 - 25;
                    var btn_close_top = $(window).height()/2-$("iframe.opsis_all_video").height()/2 - 18;
                    $("a.btn_close").css({'left': btn_close_left+'px','top': btn_close_top+'px'});
                    $('a.btn_close').fadeIn();

                }

                return false;
            });// fin du clic sur thumbnail
        // script de redimmensionnement auto des thumbnails
            $(document).ready(function(){
                var tHeight;
                $('#opsis-thumbs li img').each(function(){
                    var uHeight = $(this).css('height');
                    //console.log(uHeight);
                    if (tHeight == undefined && uHeight != '0px'){
                        tHeight=uHeight;
                    } else if (tHeight > uHeight && uHeight !='0px') {
                        tHeight=uHeight;
                    }
                });
                //console.log(tHeight);
                $('#opsis-thumbs li img').css('max-height',tHeight);

                $(window).resize(function(){
                    var tHeight;
                    //console.log(tHeight);
                    $('#opsis-thumbs li img').each(function(){
                        var uHeight = $(this).css('height');
                        if (tHeight == undefined && uHeight != '0px'){
                            tHeight=uHeight;
                        } else if (tHeight > uHeight && uHeight != '0px') {
                            tHeight=uHeight;
                        }
                    });
                    //console.log(tHeight);
                    $('#opsis-thumbs li img').css('max-height',tHeight);
                });
            });// fin du script de redimensionnement des thumbs


        //scripts de fermeture de la popin de visionnage
            function close_popin_player(){
                try {
                    if(typeof(myVideo)!='undefined') {
                        if (typeof(myVideo.destroy)=='function') {
                            myVideo.destroy();
                            myVideo = null;
                        }
                    }
                } catch (e) {
                    console.log('crash destory : '+e);
                }

                $('#fade').fadeOut();
                $('iframe.opsis_all_video').fadeOut();
                $('a.btn_close').fadeOut();
                var opsis_iframe = $('iframe.opsis_all_video');
                var opsis_container = opsis_iframe.parent();
                opsis_iframe.remove();
                opsis_iframe.attr('src','');
                opsis_container.append(opsis_iframe);

/*                if(typeof(myVideo)!='undefined') {
                    myVideo.StopTheVideo(); //stop la lecture de la video à la fermeture de la popin
                }*/
                return false;
            }
            $('#fade').on('click', close_popin_player);
            $("#btn_close").on('click', close_popin_player);


        })(jQuery);

    </script>

<?php
}

// Hook into footer so gallery becomes active after page loads
add_action('wp_footer','activate_opsis_media');



// Note importante
// Les instructions echo font planter l'affichage WP si les 'echo' ne sont pas dans un buffer
// Il faut les remplacer par return si possible ou utiliser la bufferisation ob_start() comme ci-dessous

function opsis_media($attr=array()) {
    if (isset($attr['code']) && $attr['code']!=''){
        if($attr['code'] == "live") $code="live";
        else $code=$attr['code'];
    }

    $language=opsis_blogLanguage();

    if(empty($code)){ // Player mosaïque de video
        $medias=opsis_getMedia();
        $data=opsis_getAllVideos();
        if (!empty($data['rows'])) {
            $rows=$data['rows']['row'];
            if ( is_user_logged_in() ) {
                $current_user = wp_get_current_user();
                $current_user_email = $current_user->data->user_email;
            } else {
                $current_user_email = '';
            }

            // Retrieve the items that match our query; in this case, images attached to the current post.
            // If any images are attached to the current post, do the following:
            if ($rows && $current_user_email != '') { // si on a des résultats et que l'utilisateur n'est pas blacklisté
                ob_start();
                // Initialize a counter so we can keep track of which image we are on.
                $count = 0;

                // Now we loop through all of the images that we found
                foreach ($rows as $row) {
                    // Below here are the main containers and first large image; stuff we will only want to output one time.
                    if($count == 0) { ?>

                        <!-- Whole Gallery container (inludes thumbnails) -->
                        <div id="opsis-media">

                            <!-- Main Display Area -->
                            <div id="opsis-main">

                                <!-- Affichage du player -->

                                <?php echo '<iframe class="opsis_all_video" src="" width="720" height="405" frameborder="0" allowfullscreen="" title="'.$row['titre'].'"></iframe><a href="#" class="btn_close" id="btn_close" title="Close"></a>'; ?>

                            <!-- Close the main display area -->
                            </div>

                            <!-- Formulaire de selection de video -->
                            <br>
                            <div id="filtre_video">
                                <label for="select_text" style="margin-right:9px;"><?php opsis_lang($language,"search");?></label><input type="text" id="select_text" />
                                <label for="select_media" style="margin: 0 9px 0 15px;"><?php opsis_lang($language,"media");?></label>
                                <select id="select_media">
                                    <option value="ALL" selected><?php opsis_lang($language,"all");?></option>
                                    <?php
                                        foreach ($medias as $key => $media) {
                                            echo '<option value="'.$media['value'].'">'.$media['value'].'</option>';
                                        }
                                    ?>
                                </select>
                                <input type='hidden' name='page' id='id-result-page' value='1' />
                                <input type="submit" value="<?php opsis_lang($language,"filter");?>" id="filter_media_form_fo" class="button" style="margin: 1px 24px;" />
                            </div>

    						<!-- Open the Thumbnail navigation -->
    						<div id="opsis-thumbs">
    				<!-- End the block of stuff that we only do for the first image  -->
    				<?php } ?>
    						<div id="opsis-thumb-<?php echo $count+1; ?>" class="vignette">
    							<?php
                                    if (empty($row['vignette'])) { //si pas de vignette
                                        $row['vignette']='';
                                        $class=' video_disabled';
                                        $vignette =OPSIS_URL.'/design/images/vignette_blank.png';
                                        //var_dump($vignette);
                                    } else {
										$vignette = substr($row['vignette'],strpos($row['vignette'],'/storyboard/'));
										$vignette = OPSIS_URL.'/makeVignette.php?image='.$vignette.'&w=214&h=120&kr=1&ol=player';
                                        $class=' video_enabled';
                                    }
                                    $hash_key=md5($row['code'].$language.FROM.PLAYER_EXPORT_SECRET_KEY); // clé de securisation du player_export
                                    echo '<img src="'.$vignette.'" id="'.$row['code'].'" class="thumbnail'.$class.'" alt="'.$row['titre'].'" title="'.$row['titre'].'" data-hash="'.$hash_key.'" data-email="'.$current_user_email.'"/>';
                                    echo '<div class="titre_vignette">'.$row['titre'].'</div>';
                                ?>
                            </div>
                    <?php $count = $count + 1; } ?>
                        <!-- Close the thumbnail navigation list -->
                        </div>
                    <!-- Close the entire Gallery -->
                    </div>

                    <?php if ($data['nbRows'] > NBR_LIGNES ) { ?>
                        <!-- Table de pagination -->
                        <table id="nav-table" width="100%">
                            <tr>
                                <td width="33%"></td>
                                <td width="33%" style="text-align:center;">
                                    <a href="#" title="<?php opsis_lang($language,"previous_page");?>" style="visibility:hidden;" id="prev-result"><span class="prev-icon"></span></a>
                                    <span id="current-page">1</span>/<span id="count-page"><?=$data['nb_pages']?></span>
                                    <a href="#" title="<?php opsis_lang($language,"next_page");?>" id="next-result"><span class="next-icon"></span></a>
                                </td>
                                <td width="33%"></td>
                            </tr>
                        </table>
                    <?php } ?>

                    <!-- Fond d'écran grisé à l'affichage de la popin -->
                    <div id="fade"></div>

               <?php
                $output = ob_get_contents();
                ob_end_clean();
                return $output;
            }
        }
    } else { // Player unitaire de video
        if($code == "live"){
            $data['rows']=array();
            $data['rows']['row']['code'] = "live";
            $data['rows']['row']['titre'] = "live";
        } else {
            $data=opsis_getDoc($code);
        }

        // If any images are attached to the current post, do the following:
        if (!empty($data['rows'])){
            $row=$data['rows']['row'];
            if ($row) {
                //Mise en buffer pour affichage correct dans WP du ou des players
                ob_start();
                ?>
                    <!-- Whole Gallery container (inludes thumbnails) -->
                    <div id="opsis-media">

                        <!-- Main Display Area -->
                        <div id="opsis-main">

                        <?php

                            $hash_key=md5($row['code'].$language.FROM.PLAYER_EXPORT_SECRET_KEY); // clé de securisation du player_export
                            $blog_id = get_current_blog_id();

                            if ( is_user_logged_in() ) {
                                $current_user = wp_get_current_user();
                                $user_mail = $current_user->data->user_email;
                                $user_id = $current_user->data->ID;

                                echo '<iframe class="opsis_one_video" width="720" height="405" id="video-'.$row['code'].'" src="'.OPSIS_URL.'/player_export.php?code='.$row['code'].'&play=false&language='.$language.'&email='.$user_mail.'&h='.$hash_key.'&r='.FROM.'&blog_id='.$blog_id.'&title='.$row['titre'].'&user_id='.$user_id.'" frameborder="0" allowfullscreen=""></iframe>';


                            } else {
                                echo '<iframe class="opsis_one_video" width="720" height="405" id="video-'.$row['code'].'" src="'.OPSIS_URL.'/player_export.php?code='.$row['code'].'&play=false&language='.$language.'&h='.$hash_key.'&r='.FROM.'&blog_id='.$blog_id.'&title='.$row['titre'].'&user_id=0" frameborder="0" allowfullscreen=""></iframe>';
                            }
                        ?>
                        <!-- Close the main display area -->
                        </div>
                <!-- Close the entire Gallery -->
                </div>
            <?php
                $output = ob_get_contents();
                ob_end_clean();
                return $output;
            }
        }
    }
}

function opsis_media_panier($attr=array()) {

    $language=opsis_blogLanguage();

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        $current_user_email = $current_user->data->user_email;
    }

    if ( isset($current_user_email) && $current_user_email!='' ) {

        $data=opsis_getPanier($current_user_email);

        if (!empty($data['rows'])) {

            if(isset($data['rows']['row'][0]) && $data['rows']['row'][0]!='') { // structure de donnée différente si 1 ou plusieurs résultat -> mise en forme des données différentes pour l'affichage ci-dessous
                $rows=$data['rows']['row'];
            } else {
                $rows=$data['rows'];
            }

            // Retrieve the items that match our query; in this case, images attached to the current post.
            // If any images are attached to the current post, do the following:
            if ($rows && $current_user_email != '') { // si on a des résultats et que l'utilisateur n'est pas blacklisté

                $tab_codes= array();

                ob_start();
//                echo'<pre>';
//                var_dump($data);
//                echo'</pre>';
                // Now we loop through all of the images that we found
                foreach ($rows as $idx => $row) {
                    $tab_codes[] = $row['code'];
                    // Below here are the main containers and first large image; stuff we will only want to output one time.
                    if($idx == 0) { ?>

                        <!-- Whole Gallery container (inludes thumbnails) -->
                        <div id="opsis-media">

                        <!-- Main Display Area -->
                        <div id="opsis-main">

                        <!-- Affichage du player -->

                        <?php echo '<iframe class="opsis_all_video" src="" width="720" height="405" frameborder="0" allowfullscreen="" title="'.$row['titre'].'"></iframe><a href="#" class="btn_close" id="btn_close" title="Close"></a>'; ?>  <!--   -->

                        <!-- Close the main display area -->
                        </div>


                        <!-- Open the Thumbnail navigation -->
                        <div id="opsis-thumbs">

                    <!-- End the block of stuff that we only do for the first image  -->
                    <?php } ?>

                    <!-- Now, for each of the thumbnail images, label the LI with an ID of the appropriate thumbnail number -->
                    <div id="opsis-thumb-<?php echo $idx+1; ?>" class="vignette">

                    <!-- Output a thumbnail-sized version of the image that has the attributes defined above -->
                    <?php
                        $hash_key=md5($row['code'].$language.FROM.PLAYER_EXPORT_SECRET_KEY); // clé de securisation du player_export
                        if (empty($row['vignette'])) { //si pas de vignette
                            $row['vignette']='';
                            $class=' video_disabled';
                            $vignette =OPSIS_URL.'/design/images/vignette_blank.png';
                            //var_dump($vignette);
                        } else {
							$vignette = substr($row['vignette'],strpos($row['vignette'],'/storyboard/'));
                           	$vignette =OPSIS_URL.'/makeVignette.php?image='.$vignette.'&w=214&h=120&kr=1&ol=player';
                            $class=' video_enabled';
                        }
                        echo '<img src="'.$vignette.'" id="'.$row['code'].'" class="thumbnail'.$class.'" alt="'.$row['titre'].'" title="'.$row['titre'].'" data-hash="'.$hash_key.'" data-email="'.$current_user_email.'"/>';
                        echo '<div class="titre_vignette">'.$row['titre'].'</div>';
                    ?>
                    </div>

                <?php } //endforeach ?>

                <!-- Close the thumbnail navigation list -->
                </div>

            <!-- Close the entire Gallery -->
            </div>

            <!-- Fond d'écran grisé à l'affichage de la popin -->
            <div id="fade"></div>

            <?php
                $output = ob_get_contents();
                ob_end_clean();
                return $output;
            }
        }
    }
}

// Create the Shortcode
    add_shortcode('opsis_media', 'opsis_media');
    add_shortcode('opsis_media_panier', 'opsis_media_panier');


?>
