<?php

// Register settings page under the WordPress settings menu
add_action( 'admin_menu', 'opsis_media_menu' );
function opsis_media_menu() {
	add_options_page( __('Opsis Media Options','menu-opsis'), __('Opsis Media Options','menu-opsis'), 'manage_options', 'opsis-media-settings', 'opsis_media_admin' );
	add_action( 'admin_init', 'register_opsis_settings' );
}

// Render settings page
function opsis_media_admin() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
	<div class="wrap">
	<h2>Opsis Media</h2>
	<form method="post" action="options.php">
		<?php settings_fields( 'opsis-settings-group' ); ?>
		<?php do_settings_sections( 'opsis_media' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">
					<label for="opsis_media_url">Opsis Media URL</label>
				</th>
				<td>
					<input type="text" class="regular-text" name="opsis_media_url" id="opsis_media_url" value="<?php echo get_option('opsis_media_url') ?>">
                    <span class="description">e.g. http://demo.opsismedia.com</span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
				<label for="opsis_media_key">API key</label>
				</th>
				<td>
				<input type="text" class="regular-text" name="opsis_media_key" id="opsis_media_key" value="<?php echo get_option('opsis_media_key') ?>">
				<span class="description">Please enter a valid API key</span>
				</td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		</p>
	</form>
	</div>
	<?php
}

// Register OPS settings with WordPress
function register_opsis_settings() {
	register_setting( 'opsis-settings-group', 'opsis_media_url' );
	register_setting( 'opsis-settings-group', 'opsis_media_key' );
	add_settings_section( 'opsis_media', 'Opsis Media Settings', 'f_opsis_media_settings_section', 'opsis_media' );
}

// Render section content for OPS section
function f_opsis_media_settings_section() {
	?>
	<p>
		Provide connection URL for Opsis Media site.
	</p>
	<?php
}
?>
