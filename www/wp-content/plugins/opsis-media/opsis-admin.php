<?php

    //Assign a name to your tab
    function upload_opsis_media_menu($tabs) {
        $tabs['opsis_upload']='Insert from Opsis Media';
        return $tabs;
    }

    //Adds your scripts to your plugin
    function upload_opsis_scripts() {
        //Adds css
        $myStyleUrl = plugins_url('opsis-upload-media.css', __FILE__);
        wp_register_style('myStyleSheets', $myStyleUrl);
        wp_enqueue_style( 'myStyleSheets');

        //Adds your custom script
        wp_deregister_script( 'upload-media-script' );
        //wp_register_script( 'upload-media-script', '/wp-content/plugins/upload-media/opsis-upload-media.js');
        wp_register_script( 'upload-media-script', plugins_url('opsis-upload-media.js', __FILE__));
        wp_localize_script('upload-media-script', 'WPURLS', array( 'siteurl' => get_option('siteurl'), 'plugin_url' => plugins_url(), 'OPSIS_URL' => OPSIS_URL, 'OPSIS_KEY' => OPSIS_KEY, 'NBR_LIGNES' => NBR_LIGNES, 'CURRENT_LANG' => opsis_blogLanguage(), 'No_result'=> __('No results')));
        wp_enqueue_script( 'upload-media-script' );
    }

    //This is our form for the plugin
    function upload_opsis_upload_form () {
        //echos the tabs at the top of the media window
        media_upload_header();

        //Adds your javascript
        upload_opsis_scripts();

        //formulaire de filtre
        $medias=opsis_getMedia();

        echo'<div id="filtre_video">';
        echo'<label for="select_text" style="margin-right:9px;">Recherche</label><input type="text" id="select_text" />';
        echo'<label for="select_media" style="margin: 0 15px;">Media</label>';
        echo '<select id="select_media">';
        echo '<option value="ALL" selected>Tous</option>';
        foreach ($medias as $key => $media) {
            echo '<option value="'.$media['value'].'">'.$media['value'].'</option>';
        }
        echo '</select>';
        echo "<input type='hidden' name='page' id='id-result-page' value='1' />";
        echo '<input type="submit" value="Filtrer" id="filter_media_form" class="button" style="margin: 1px 24px;" />';
        echo '</div>';
        $data=opsis_getAllVideos();
        $rows=$data['rows']['row'];
        echo '<div>
        <ul id="opsis-thumbs">';

        foreach ($rows as $idx=>$row) {
            if($idx==0){ $class="thumb selected";}
            else {$class="thumb";}
            //if ($row['vignette'] !='storyboard/ / '){
                if (empty($row['vignette'])) { //si pas de vignette
                    $row['vignette']='';
                    $vignette =OPSIS_URL.'/design/images/vignette_blank.png';
                    $class.=" blank_bo";
                } else {
					$vignette = substr($row['vignette'],strpos($row['vignette'],'/storyboard/'));
                    $vignette = OPSIS_URL.'/makeVignette.php?image='.$vignette.'&w=160&h=90&kr=1';
                }
                echo '<li id="opsis-thumb-'.($idx+1).'">';
                echo '<img src="'.$vignette.'" id="'.$row['code'].'" class="'.$class.'" title="'.$row['titre'].'"/>';
                echo '<p>'.$row['titre'].'</p>';
                echo '</li>';
            //}
       }
        echo '</ul>
        </div>';

        if ($data['nbRows'] > NBR_LIGNES ) {
            echo '<table id="nav-table" width="100%">
                    <tr>
                        <td width="33%"></td>
                        <td width="33%" style="text-align:center;">
                            <a href="#" title="Page précédente" style="visibility:hidden;" id="prev-result"><span class="prev-icon"></span></a>
                            <span id="current-page">1</span>/<span id="count-page">'.$data['nb_pages'].'</span>
                            <a href="#" title="Page suivante" id="next-result"><span class="next-icon"></span></a>
                        </td>
                        <td width="33%"></td>
                    </tr>
                </table>';
        }

        echo "<div class='insert-form'>
                <input id='insert_shortcode' type='button' class='button' value='Insert Media' style='margin-left: 25px' />
            </div> ";

    }

    //Returns the iframe that your plugin will be returned in
    function upload_opsis_menu_handle() {
        return wp_iframe('upload_opsis_upload_form');
    }

    //Needed script to make sure wordpresses media upload scripts are inplace

    function upload_opsis_back(){
        wp_enqueue_script('media-upload');
    }

    //Adds your tab to the media upload button
    add_filter('media_upload_tabs', 'upload_opsis_media_menu');

    //Adds your menu handle to when the media upload action occurs
    add_action('media_upload_opsis_upload', 'upload_opsis_menu_handle');
    add_action('wp_enqueue_scripts', 'upload_opsis_back');

