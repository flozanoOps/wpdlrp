<?php


//require_once('../../../../wp-config.php');

/* fonction de récupération de la langue utilisé par le blog WP */
function opsis_blogLanguage(){
    if (class_exists('MslsBlogCollection')){
         $blog     = MslsBlogCollection::instance()->get_current_blog();
        $language = strtoupper($blog->get_language());
    } else {
        //$language = 'FR';
        $language = get_bloginfo('language');
    }
/*    var_dump($language);
    exit;*/
    $language=strtoupper(substr($language,0,2));

    $array_langues=array('FR','EN','DE','IT','ES','NL');
    if (in_array($language, $array_langues)){
        return $language;
    } else {
        return 'EN';
    }
}


/* fonction de traduction de wording defini dans opsis-media-constantes*/
function opsis_lang($lang,$word) {
    global $str_lang;
    echo $str_lang[$lang][$word];
}

//-------------------------------------------------------------
// Opsis Media by Opsomai
//-------------------------------------------------------------

/* fonctions de recupération de data de la plateforme dlrp */

/* recuperation de toutes les videos */
    function opsis_getAllVideos(){

        $param['tri'] = "doc_date_prod1";
        $langue = opsis_blogLanguage();
        $param['nbLignes'] = NBR_LIGNES;
		$param['api_key'] = OPSIS_KEY;
        $str_post = http_build_query($param);

        $curl=curl_init(OPSIS_URL."/".OPSIS_API."?urlaction=recherche&lang=".$langue);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

		// Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $data=$rsp['data'];
        return $data;
    }

/* recuperation d'un panier en fonction de l'email de son propriétaire */
    function opsis_getPanier($email){

        $param['tri'] = "doc_date_prod1";
        $param['email'] = $email;
        $langue = opsis_blogLanguage();
        $param['nbLignes'] = 100; //NBR_LIGNES;
        $str_post = http_build_query($param);

        $curl=curl_init(OPSIS_URL."/wp-panier.php?lang=".$langue);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $data=$rsp['data'];
        return $data;
    }

/* recupération d'une video */
    function opsis_getDoc($code){

		/*
        $param['chFields'][1] = "ID_DOC";
        $param['chTypes'][1] = "CE";
        $param['chValues'][1] = $id;
        $param['chNoHLs'][1] = "1";
        */
		$param['chField'][1] = "code";
		$param['chValue'][1] = $code;

        $param['tri'] = "doc_date_prod1";
		$param['api_key'] = OPSIS_KEY;
       	$str_post = http_build_query($param);

        $langue = opsis_blogLanguage();
        $curl=curl_init(OPSIS_URL."/".OPSIS_API."?urlaction=recherche&lang=".$langue);//pas nécéssaire

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $rows=$rsp['data'];
        return $rows;
    }

/* recuperation des medias  */

    function opsis_getMedia(){
        $param['nbLignes'] = "10";
		$param['api_key'] = OPSIS_KEY;
       	$str_post = http_build_query($param);

        $curl=curl_init(OPSIS_URL."/".OPSIS_API."?urlaction=listevaleurs&type=CT_MEDIA");

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $str_post);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        // Conversion xml en tableau
        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $rsp = json_decode($json,TRUE);

        $rows=$rsp['data']['rows']['row'];
        return $rows;
    }
